<?php

use App\Container;

require __DIR__ . '/../vendor/autoload.php';

$app = Container::getInstance();


switch ($_SERVER['REQUEST_URI']) {
    case '/':
        return (new \App\Http\Controllers\IndexController())->index();
        break;
    default:
        return '404';
}

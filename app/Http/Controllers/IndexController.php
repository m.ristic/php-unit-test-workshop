<?php

namespace App\Http\Controllers;

use App\Inc\Response;

class IndexController
{
    public function index()
    {
        return (new Response())->toJson([
            'success' => 'true',
            'message' => 'welcome to the homepage'
        ]);
    }
}

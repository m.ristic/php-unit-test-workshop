<?php

namespace App;

class Container
{
    private static $container = null;

    public static function getInstance()
    {
        if (self::$container == null) {
            self::$container = new Container();
        }
        return self::$container;
    }

    protected function __clone()
    {
    }

    protected function __construct()
    {
    }
}

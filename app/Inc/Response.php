<?php

namespace App\Inc;

class Response
{
    private $content;

    public function __construct(string $content = '', int $code = 200)
    {
        $this->content = $content;
        http_response_code($code);
        return $this;
    }

    public function toJson(array $content = [], int $code = 200)
    {
        header('Content-type: application/json');
        echo json_encode($content);
    }


}
